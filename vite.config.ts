import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/

const path = require('path')

export default defineConfig({
  plugins: [react()],

  build: {
      lib: {
          // 번들링 진입점은 main.ts, 파일 이름은 ToDoList로
          entry: path.resolve(__dirname, 'src/main.tsx'),
          name: 'ToDoList',
          fileName: (format) => `ToDoList.${format}.js`
      },
      rollupOptions: {
            // make sure to externalize deps that shouldn't be bundled
            // into your library
            external: ['react'],
            output: {
              // Provide global variables to use in the UMD build
              // for externalized deps
              globals: {
                react: 'React'
              }
            }
      }
  }
})