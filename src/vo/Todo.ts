class ToDo {
    id: string = '';
    task: string = '';
    status: boolean = false;

    constructor(id: string, task: string, status: boolean) {
        this.id = id;
        this.task = task;
        this.status = status;
    }

}

export default ToDo