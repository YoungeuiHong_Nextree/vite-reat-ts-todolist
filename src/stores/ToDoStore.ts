import ToDo from "../vo/Todo";

class ToDoStore {

    todos:ToDo[] = [];

    createTodo(ToDo: ToDo) {
        this.todos.push({...ToDo});
    }

    readTodos() {
        return this.todos;
    }

    updateTodoStatus(id: string) {
        this.todos.map((todo) => {
            if (todo.id === id) {
                todo.status = !todo.status;
            }
        });
    }

    deleteTodo(id: string) {
        const targetIndex = this.todos.findIndex((todo) => todo.id === id);
        this.todos.splice(targetIndex,1);
    }

}
export default ToDoStore;