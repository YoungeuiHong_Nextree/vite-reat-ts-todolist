import { List,ListItem, ListItemButton, ListItemIcon, ListItemText, Checkbox } from '@mui/material';

function ToDoList(){

    let todos = [
        {id: 1, title: '책 읽기', done: false},
        {id: 2, title: '청소하기', done: true}
    ]

    return (
        <List>
            {todos.map((todo) => {
                return(
                    <ListItem>
                        <ListItemButton>
                            <ListItemIcon>
                                <Checkbox/>
                            </ListItemIcon>
                            <ListItemText primary={todo.title}/>
                        </ListItemButton>
                    </ListItem>
                );
            })}
        </List>
    );
}

export default ToDoList;