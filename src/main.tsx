import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {Provider} from 'mobx-react';
import ToDoStore from './stores/ToDoStore'

ReactDOM.render(
  <Provider todoStore = {ToDoStore}>
    <App />
  </Provider>,
  document.getElementById('root')
)
