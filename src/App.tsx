import { useState } from 'react'
import Todo from './vo/Todo'

function App() {
    const todo = new Todo(1, 'todo-1', false);

    return (
        <div>
            <h3>to-do list</h3>
        </div>
    )
}

export default App
